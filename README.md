
dgro_asub
======
European Spallation Source ERIC Site-specific EPICS module : dgro_asub


To compile this module as an E3 module:

* Install `conda` (https://docs.conda.io/en/latest/miniconda.html)
* Create and activate a conda environment that minimally includes the following packages: `make perl tclx compilers epics-base require`
* Run `make -f Makefile.E3 target` to build `target` using the E3 build process
