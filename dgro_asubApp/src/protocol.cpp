#include "dgro_asub/protocol.h"
// #include <waveformRecord.h>
// #include <dbCommon.h>
// #include <dbLink.h>
// #include <dbAccessDefs.h>
#include <alarm.h>
#include <recGbl.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <sstream>
#include <iostream>

namespace dgro {

  WordVector encode(WordVector addresses, WordVector data) {
    size_t n = (addresses.size() == data.size()) ?
      addresses.size() : throw std::runtime_error(
        "address vector and data vector have different lengths"
      );
    WordVector result;
    for (size_t i = 0; i < n; ++i) {
      result.push_back(addresses[i]);
      result.push_back(data[i]);
    }

    return result;
  }

  void check_type(const char* recordname, const char* target, auto type) {
    if (type != ::dgro::epicsDBFType) {
      std::stringstream ss;
      ss << recordname
      << ": expected " << pamapdbfType[::dgro::epicsDBFType].strvalue
      << " for " << target << ", but got " << pamapdbfType[type].strvalue;
      throw std::runtime_error(ss.str());
    }
  };

  std::tuple<WordVector,WordVector> unpack(aSubRecord* asub) {
    check_type(asub->name, "inpa", asub->fta);
    check_type(asub->name, "inpb", asub->ftb);

    if (asub->nea != asub->neb) {
      std::stringstream ss;
      ss << asub->name << ": inpa and inpb do not have the same size";
      throw std::runtime_error(ss.str());
    }

    word* a = (word*) asub->a;
    word* b = (word*) asub->b;
    return std::make_tuple(
      WordVector(a, a + asub->nea),
      WordVector(b, b + asub->neb)
    );
  }

  void pack(aSubRecord* asub, WordVector r) {
    check_type(asub->name, "vala", asub->ftva);
    if (r.size() > asub->nova) {
      std::stringstream ss;
      ss << asub->name << ": nova < (nea + neb): output too small to hold result ";
      throw std::runtime_error(ss.str());
    }
    asub->neva = r.size();
    std::copy(std::begin(r), std::end(r), (dgro::word*) asub->vala);
  }
}

int dgroEncode(aSubRecord* asub) {
  try {
    using namespace dgro;
    WordVector a, b;
    std::tie(a,b) = unpack(asub);
    WordVector r = encode(a,b);
    pack(asub, r);
    return 0;
  } catch(const std::exception& e) {
    std::cerr << e.what() << "\n";
  } catch(...) {
    std::cerr <<"unknown exception \n";
  }
  recGblSetSevr(asub, SOFT_ALARM, INVALID_ALARM);
  return 1;
}

epicsRegisterFunction(dgroEncode);
