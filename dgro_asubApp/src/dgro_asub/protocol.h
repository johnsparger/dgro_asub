#ifndef DGRO_PROTOCOL_H
#define DGRO_PROTOCOL_H

#include <aSubRecord.h>
#include <dgro_asub/dbfldtypes_wrapper.h>
#include <cstdint>
#include <vector>
#include <tuple>

namespace dgro {
  using word = epicsUInt32;
  using WordVector = std::vector<word>;
  const dbfType epicsDBFType = DBF_ULONG;

  using std::begin;
  using std::end;

  // Interleave the addresses and data into a single vector.
  WordVector encode(WordVector addresses, WordVector data);

  // Get data out of asub and into WordVectors
  std::tuple<WordVector,WordVector> unpack(aSubRecord* asub);

  // Put result into asub output
  void pack(aSubRecord* asub, WordVector r);
}

int dgroEncode(aSubRecord* asub);

#endif
