// This is a bit of grossness so I can get access to pamapdbfType without
// any nasty warnings.

// 1. include guard
// 2. define DBFLDTYPES_GBLSOURCE to get pamapdbfType, which has a string name
// for each dbfType enum value.
// 3. change char to const char to get rid of this warning when compiling under c++:
// dbFldTypes.h:72:1: warning: ISO C++ forbids converting a string constant to 'char*' [-Wwrite-strings]
// 4. don't include shareLib.h as it is not needed and I don't want to
// replace char with const char in that file. Who knows what that would do.
// Making dbFldTypes.h include our wrapper instead will be a no-op because of
// the include guard.


#ifndef dbfldtypes_wrapper // 1.
#define dbfldtypes_wrapper

#define DBFLDTYPES_GBLSOURCE // 2.
#define char const char // 3.
#define shareLib dbfldtypes_wrapper // 4.
  #include <dbFldTypes.h>
#undef shareLib
#undef char
#undef DBFLDTYPES_GBLSOURCE

#endif
