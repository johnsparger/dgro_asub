FROM registry.esss.lu.se/ics-docker/conda-build:latest
MAINTAINER ESS ICS WP12

# create the envionrment in such a way that it can be cached.
WORKDIR /home/conda
COPY --chown=conda:wheel environment.yml /home/conda/environment.yml
RUN conda config --set channel_alias https://artifactory.esss.lu.se/artifactory/api/conda && \
conda env create --force -n ioc -f environment.yml

# put conda setup commands in ~/.bashrc
RUN conda init

# build the library
COPY --chown=conda:wheel . /home/conda/dgro_asub
WORKDIR /home/conda/dgro_asub
RUN eval "$(/opt/conda/condabin/conda shell.bash hook)" && \
conda activate ioc && \
make cleaner && \
make install

# use an entrypoint script and the exec form of CMD so the IOC can be
# stopped gracefully
CMD ["/home/conda/dgro_asub/test/services/ioc/entrypoint.sh"]
