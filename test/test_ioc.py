import pytest
import pvaccess
from .test_simulator import sim
import time
import struct

pytest_plugins = ["docker_compose"]

DGRO_HEADER = "e55c"
READ_REQUEST = "0001"
READ_RESPONSE = "0002"
WRITE_REQUEST = "0003"
WRITE_RESPONSE = "0004"
BAD_HEADER = "1001"
BAD_TYPE = "1002"

def word(x):
    '''convert a list or value into bytestring of 32bit word(s)'''
    try:
        return struct.pack(">{}I".format(len(x)), *x)
    except TypeError:
        return struct.pack(">I", x)

def pvget(channel):
    return channel.get()["value"]

def assert_pv_array_equal(channel, expected):
    assert (pvget(channel) == expected).all()

def assert_alarm_equal(channel, expected):
    assert (channel.get()["alarm"] == expected)

NO_ALARM = {'severity': 0, 'status': 0, 'message': 'NO_ALARM'}
SOFT_ALARM = {'severity': 3, 'status': 3, 'message': 'SOFT'}

class IOC:
    def __init__(self):
        # the PVs of the IOC
        self.pv_names = ["Addresses", "Data", "my_asub_record", "Encode"]

        # wait for the IOC to come online
        c = pvaccess.Channel(self.pv_names[0])
        c.setTimeout(30);
        c.get()

        # creaete the channels
        self.addresses, self.data, self.asub, self.encode = [pvaccess.Channel(n) for n in self.pv_names]

@pytest.fixture(scope="function")
def ioc(function_scoped_container_getter):
    return IOC()

def test_put(ioc):
    x = [1,2,3]
    ioc.addresses.put(x)
    assert_pv_array_equal(ioc.addresses, x)

def test_encode_result(ioc):
    ioc.addresses.put([1,2,3])
    ioc.data.put([4,5,6])
    assert_pv_array_equal(ioc.encode, [1,4,2,5,3,6])
    assert_alarm_equal(ioc.asub, NO_ALARM)

def test_alarm_on_different_sized_inputs(ioc):
    ioc.addresses.put([1,2,3])
    ioc.data.put([4,5])
    assert_alarm_equal(ioc.asub, SOFT_ALARM)

def test_stream_coms(ioc, sim):
    with sim:
        a = [1,2,3]
        b = [4,5,6]
        c = [1,4,2,5,3,6]
        ioc.addresses.put(a)
        ioc.data.put(b)
        time.sleep(1)
        device = sim.get_object("device")
        exchange = device.get_last_exchange()
        expected = {
            "message": DGRO_HEADER + WRITE_REQUEST + word(c).hex(),
            "response": DGRO_HEADER + WRITE_RESPONSE + word(c).hex()
        }
        assert exchange == expected
