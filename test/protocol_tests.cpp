#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <dgro_asub/protocol.h>
#include <iostream>
#include <aSubRecord.h>
#include <dbFldTypes.h>

// from gtest:
using ::testing::Test;
using ::testing::TestWithParam;
using testing::WithParamInterface;

// from gmock:
using ::testing::ElementsAreArray;

// from dgro:
using dgro::WordVector;

struct EncodeTestParams {
  WordVector addresses;
  WordVector data;
  WordVector expected_result;

  friend std::ostream& operator<<(std::ostream& os, const EncodeTestParams& p) {
    return os
    << "{addresses: " << ::testing::PrintToString(p.addresses)
    << ", data: " << ::testing::PrintToString(p.data)
    << ", expected_result: " << ::testing::PrintToString(p.expected_result)
    << "}";
  }
};

struct EncodeFixture : public TestWithParam<EncodeTestParams> {};

TEST_P(EncodeFixture, CheckEncodeResult) {
  EncodeTestParams p = GetParam();
  ASSERT_EQ(dgro::encode(p.addresses, p.data), p.expected_result);
}

TEST_F(EncodeFixture, ThrowOnDifferentSizeInputs) {
  ASSERT_THROW(dgro::encode({1}, {1,2,3}), std::runtime_error);
}

INSTANTIATE_TEST_SUITE_P(
  EncodeShouldInterleaveInputs,
  EncodeFixture,
  ::testing::Values(
    EncodeTestParams{{},{},{}},
    EncodeTestParams{{1},{2},{1,2}},
    EncodeTestParams{{1,3},{2,4},{1,2,3,4}},
    EncodeTestParams{{1,1,1},{0,0,0},{1,0,1,0,1,0}}
  )
);

struct ASubRecordFixture : public Test {
  ASubRecordFixture() {
    asub.a = three_words.data();
    asub.noa = three_words.size();
    asub.nea = three_words.size();
    asub.fta = dgro::epicsDBFType;
    asub.b = three_words.data();
    asub.nob = three_words.size();
    asub.neb = three_words.size();
    asub.ftb = dgro::epicsDBFType;
    asub.vala = output_storage.data();
    asub.nova = output_storage.size();
    asub.neva = 0;
    asub.ftva = dgro::epicsDBFType;
  }

  aSubRecord asub;
  WordVector output_storage = WordVector(100);
  WordVector three_words = {1,2,3};
  WordVector five_words = {1,2,3,4,5};
  std::vector<char> three_chars = {'a','b','c'};
  std::vector<double> three_doubles = {1.0,2.0,3.0};
};

struct UnpackFixture : public ASubRecordFixture {};

TEST_F(UnpackFixture, UnpackASubInputs) {
  ASSERT_EQ(dgro::unpack(&asub), std::make_tuple(three_words, three_words));
}

TEST_F(UnpackFixture, UnpackThrowsOnDifferentTypes) {
  asub.b = three_chars.data();
  asub.ftb = DBF_CHAR;
  ASSERT_THROW(dgro::unpack(&asub), std::runtime_error);
}

TEST_F(UnpackFixture, UnpackThrowsOnDifferentSizes) {
  asub.b = five_words.data();
  asub.nob = five_words.size();
  asub.neb = five_words.size();
  ASSERT_THROW(dgro::unpack(&asub), std::runtime_error);
}

TEST_F(UnpackFixture, UnpackThrowsOnWrongType) {
  asub.a = three_chars.data();
  asub.fta = DBF_CHAR;
  asub.b = three_chars.data();
  asub.ftb = DBF_CHAR;
  ASSERT_THROW(dgro::unpack(&asub), std::runtime_error);
}

struct PackFixture : public ASubRecordFixture {};

void check_asub_output(const aSubRecord& asub, const WordVector& expected_result) {
  auto vala = (dgro::word*) asub.vala;
  ASSERT_THAT(WordVector(vala, vala + asub.neva), ElementsAreArray(expected_result));
}

TEST_F(PackFixture, CheckPackedValues) {
  dgro::pack(&asub, three_words);
  check_asub_output(asub, three_words);
}

TEST_F(PackFixture, PackThrowsIfOutputStorageTooSmall) {
  asub.nova = three_words.size()-1;
  ASSERT_THROW(dgro::pack(&asub, three_words), std::runtime_error);
}

TEST_F(PackFixture, PackThrowsOnWrongType) {
  asub.ftva = DBF_CHAR;
  ASSERT_THROW(dgro::pack(&asub, three_words), std::runtime_error);
}

struct SubroutineFixture : public ASubRecordFixture,
                           public WithParamInterface<EncodeTestParams> {};

TEST_P(SubroutineFixture, CheckSubroutineResults) {
  EncodeTestParams p = GetParam();
  asub.a = p.addresses.data();
  asub.noa = p.addresses.size();
  asub.nea = p.addresses.size();
  asub.b = p.data.data();
  asub.nob = p.data.size();
  asub.neb = p.data.size();
  int r = dgroEncode(&asub);
  ASSERT_EQ(r, 0);
  check_asub_output(asub, p.expected_result);
}

INSTANTIATE_TEST_SUITE_P(
  CheckAsubOutputs,
  SubroutineFixture,
  ::testing::Values(
    EncodeTestParams{{},{},{}},
    EncodeTestParams{{1},{2},{1,2}},
    EncodeTestParams{{1,3},{2,4},{1,2,3,4}},
    EncodeTestParams{{1,1,1},{0,0,0},{1,0,1,0,1,0}}
  )
);

void subroutine_error_assertions(aSubRecord* asub) {
  int r = 0;
  ASSERT_NO_THROW(r = dgroEncode(asub));
  ASSERT_NE(r, 0);
}

TEST_F(SubroutineFixture, SubroutineErrorCodeOnDifferentTypes) {
  asub.b = three_chars.data();
  asub.ftb = DBF_CHAR;
  subroutine_error_assertions(&asub);
}

TEST_F(SubroutineFixture, SubroutineErrorCodeOnDifferentSizes) {
  asub.b = five_words.data();
  asub.nob = five_words.size();
  asub.neb = five_words.size();
  subroutine_error_assertions(&asub);
}

TEST_F(SubroutineFixture, SubroutineErrorCodeOnWrongType) {
  asub.a = three_chars.data();
  asub.fta = DBF_CHAR;
  asub.b = three_chars.data();
  asub.ftb = DBF_CHAR;
  subroutine_error_assertions(&asub);
}

TEST_F(SubroutineFixture, SubroutineErrorCodeIfOutputStorageTooSmall) {
  asub.nova = three_words.size()-1;
  subroutine_error_assertions(&asub);
}

TEST_F(SubroutineFixture, SubroutineErrorCodeOnWrongOutputType) {
  asub.ftva = DBF_CHAR;
  subroutine_error_assertions(&asub);
}
