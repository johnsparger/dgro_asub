import pytest
from lewis.core.control_client import ControlClient
import time
import socket
import struct

pytest_plugins = ["docker_compose"]

simulator_control_port = 10000
simulator_interface_port = 9999
test_udp_port = 7777

DGRO_HEADER = "e55c"
READ_REQUEST = "0001"
READ_RESPONSE = "0002"
WRITE_REQUEST = "0003"
WRITE_RESPONSE = "0004"
BAD_HEADER = "1001"
BAD_TYPE = "1002"

def word(x):
    '''convert a list or value into bytestring of 32bit word(s)'''
    try:
        return struct.pack(">{}I".format(len(x)), *x)
    except TypeError:
        return struct.pack(">I", x)

class ControlClientContext(ControlClient):
    def __init__(self, *args, **kwargs):
        super(ControlClientContext, self).__init__(*args, **kwargs)
        self.ip = kwargs["host"]

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._socket.close()

@pytest.fixture(scope="function")
def sim(function_scoped_container_getter):
    container = function_scoped_container_getter.get("sim")
    ip = container.get("NetworkSettings.Networks.dgro_asub_default.IPAddress")
    client = ControlClientContext(host=ip, port=simulator_control_port)
    return client

class SimulatorSocket():
    def __init__(self, sim):
        self.address = (sim.ip, simulator_interface_port)
        self.socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        self.socket.bind(("0.0.0.0",test_udp_port))
        time.sleep(2)

    def send(self, data):
        self.socket.sendto(data, self.address)

    def recv(self):
        return self.socket.recv(8096)

def test_device_access(sim):
    with sim:
        device = sim.get_object("device")
        assert device


def test_interface_access(sim):
    with sim:
        interface = sim.get_object("interface")
        assert interface.is_connected()["udpstream"]


def test_initial_exchange(sim):
    with sim:
        device = sim.get_object("device")
        exchange = device.get_last_exchange()
        assert exchange["message"] == None

def test_udp_coms(sim):
    with sim:
        s = SimulatorSocket(sim)
        message = DGRO_HEADER + READ_REQUEST + word(1).hex()
        s.send(bytes.fromhex(message))
        data = s.recv().hex()
        expected = DGRO_HEADER + READ_RESPONSE + word(1).hex() + word(0).hex()
        assert data == expected
