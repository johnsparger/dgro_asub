from .detector_readout import DGROParser, DGROMessage, DGROBinaryMatcher
import pytest
import struct

def word(x):
    '''convert a list or value into bytestring of 32bit word(s)'''
    try:
        return struct.pack(">{}I".format(len(x)), *x)
    except TypeError:
        return struct.pack(">I", x)

DGRO_HEADER = "e55c"
READ_REQUEST = "0001"
READ_RESPONSE = "0002"
WRITE_REQUEST = "0003"
WRITE_RESPONSE = "0004"
BAD_HEADER = "1001"
BAD_TYPE = "1002"

bad_parser_inputs = [
    ('ab', None), # test message way too short
    (DGRO_HEADER, None), # test message too short
    ("e77c"+READ_REQUEST, "header"), # test bad header
    (DGRO_HEADER+"9999", "code") # test bad type code
]

@pytest.mark.parametrize("message,prop", bad_parser_inputs)
def test_DGROParser_fail_eager(message, prop):
    with pytest.raises(ValueError):
        p = DGROParser(bytes.fromhex(message), eager=True)

@pytest.mark.parametrize("message,prop", bad_parser_inputs)
def test_DGROParser_fail_lazy(message, prop):
    with pytest.raises(ValueError):
        p = DGROParser(bytes.fromhex(message))
        x = getattr(p, prop)

def test_DGROMessage_bad_code():
    with pytest.raises(ValueError):
        m = DGROMessage(code='77')

codes_which_need_addresses = [
    READ_REQUEST,
    READ_RESPONSE,
    WRITE_REQUEST,
    WRITE_RESPONSE
]

@pytest.mark.parametrize("code", codes_which_need_addresses)
def test_DGROMessage_missing_addresses(code):
    with pytest.raises(ValueError):
        m = DGROMessage(code, addresses=[])

codes_which_need_data= [
    READ_RESPONSE,
    WRITE_REQUEST,
    WRITE_RESPONSE
]

@pytest.mark.parametrize("code", codes_which_need_data)
def test_DGROMessage_missing_data(code):
    with pytest.raises(ValueError):
        m = DGROMessage(code, addresses=[1,2,3])

def test_DGROMessage_payload():
    m = DGROMessage(READ_RESPONSE, addresses=[1,2], data=[3,4])
    assert m.payload() == [1,3,2,4]

def test_DGROMessage_hex():
    m = DGROMessage(READ_RESPONSE, addresses=[1,2], data=[3,4])
    expected = DGRO_HEADER + READ_RESPONSE + word([1,3,2,4]).hex()
    assert m.hex() == expected

too_short = [
    'ab', # test message way too short
    DGRO_HEADER, # test message too short
]

long_enough = [
    "e77c"+READ_REQUEST, # test bad header
     DGRO_HEADER+"9999", # test bad type code
     DGRO_HEADER + READ_REQUEST + word([1,2]).hex()
]

@pytest.mark.parametrize("r", too_short)
def test_DGROBinaryMatcher_too_short(r):
    b = DGROBinaryMatcher()
    assert not b.match(bytes.fromhex(r))

@pytest.mark.parametrize("r", long_enough)
def test_DGROBinaryMatcher_match(r):
    b = DGROBinaryMatcher()
    assert b.match(bytes.fromhex(r))

def test_unparse_parse_reversible():
    m1 = DGROMessage(WRITE_REQUEST, addresses=[1,2], data=[3,4])
    p = DGROBinaryMatcher().match(m1.bytes())[0]
    m2 = DGROMessage(p.code, p.addresses, p.data)
    assert m1.code == m2.code
    assert m1.addresses == m2.addresses
    assert m1.data == m2.data
    assert m1.payload() == m2.payload()
    assert m1.hex() == m2.hex()
    assert m1.bytes() == m2.bytes()
