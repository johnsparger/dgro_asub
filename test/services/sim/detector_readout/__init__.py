from lewis.devices import Device
from lewis.adapters.stream import PatternMatcher, StreamAdapter, StreamServer, StreamHandler
from lewis.adapters.stream import StreamInterface, Cmd
from lewis.core.logging import has_log
import sys
import struct
import asyncore
import asynchat
from six import b
import socket
import traceback
import functools

@has_log
class DGRO(Device):
    last_exchange = {"message": None, "response": None}

    def __init__(self):
        super(DGRO, self).__init__()
        self.memory = {}

    def log_exchange(self, message, response):
        self.last_exchange = {"message": message, "response": response}

    def get_last_exchange(self):
        return self.last_exchange

    def read(self, addresses):
        return [self.memory.get(a, 0) for a in addresses]

    def write(self, addresses, data):
        for a,d in zip(addresses,data):
            self.memory[a] = d


def word(x):
    '''convert a list or value into bytestring of 32bit word(s)'''
    try:
        return struct.pack(">{}I".format(len(x)), *x)
    except TypeError:
        return struct.pack(">I", x)

def unword(x):
    '''convert a bytestring of 32bit word(s) to integers'''
    BYTE = 4
    return list(struct.unpack(">{}I".format(int(len(x)/BYTE)), x))

@has_log
class DGROMessage:
    HEADER = 'e55c'
    OK = '00'
    ERROR = '10'
    READ_REQUEST = OK + '01'
    READ_RESPONSE= OK + '02'
    WRITE_REQUEST = OK + '03'
    WRITE_RESPONSE = OK + '04'
    BAD_HEADER = ERROR + '01'
    BAD_TYPE = ERROR + '02'
    valid_codes = [READ_REQUEST, READ_RESPONSE, WRITE_REQUEST, WRITE_RESPONSE, BAD_HEADER, BAD_TYPE]
    header_only_codes = [BAD_HEADER, BAD_TYPE]
    address_only_codes = [READ_REQUEST]
    data_codes = [READ_RESPONSE, WRITE_REQUEST, WRITE_RESPONSE]

    def __init__(self, code, addresses=[], data=[]):
        self.code = code
        self.addresses = addresses
        self.data = data
        self.check_code()
        self.check_payload()

    def check_code(self):
        if self.code not in DGROMessage.valid_codes:
            message = "expected one of these message type codes {} but got {} instead".format(DGROMessage.valid_codes, self.code)
            self.log.info("error: " + message)
            raise ValueError(message)

    def check_payload(self):
        if self.code in DGROMessage.header_only_codes:
            if self.addresses or self.data:
                raise ValueError("DGRO message type {} expects no payload but adresses={} and data={} were provided".format(self.code, self.addresses, self.data))
        elif self.code in DGROMessage.address_only_codes:
            if not self.addresses:
                raise ValueError("DGRO message type {} expects addresses in the payload, but none were provided".format(self.code))
            if self.data:
                raise ValueError("DGRO message type {} expects no data but data = {} was provided".format(self.code, self.data))
        elif self.code in DGROMessage.data_codes:
            if not self.data:
                raise ValueError("DGRO message type {} expects data in the payload, but none was provided".format(self.code))
            elif len(self.addresses) != len(self.data):
                raise ValueError("DGRO message type {} expects the length of the addresses and data to be equal. Instead len(addresses)={} and len(data)={}".format(self.code, len(addresses), len(data)))

    def payload(self):
        return [val for pair in zip(self.addresses, self.data) for val in pair]

    def hex(self):
        return self.HEADER + self.code + word(self.payload()).hex()

    def bytes(self):
        return bytes.fromhex(self.hex())


@has_log
class DGROParser:
    def __init__(self, request, eager=False):
        self.BYTE = 2
        self._request = request.hex()
        self._header = None
        self._code = None
        self._payload = None
        self._addresses = []
        self._data = []
        self._check_length()
        if eager:
            self._parse_header()
            self._parse_code()
            self._parse_message()

    @property
    def header(self):
        return self._header if self._header else self._parse_header()

    @property
    def code(self):
        return self._code if self._code else self._parse_code()

    @property
    def payload(self):
        return self._payload if self._payload else self._parse_payload()

    @property
    def addresses(self):
        if not self._payload:
            self._parse_payload()
        return self._addresses

    @property
    def data(self):
        if not self._payload:
            self._parse_payload()
        return self._data

    def _check_length(self):
        length = len(self._request)
        if length < (4*self.BYTE):
            message = "DGROParser request must be at least 4 bytes. Got message {} instead which is {} bytes".format(self._request, len(self._request)/self.BYTE)
            self.log.info("error: " + message)
            raise ValueError(message)
        elif length % (4*self.BYTE) != 0:
            message = "DGROParser request length must be a multiple of 4 bytes. Got request of length {} bytest".format(length/self.BYTE)
            self.log.info("error: " + message)
            raise ValueError(message)

    def _parse_header(self):
        self._header = self._request[0:2*self.BYTE]
        if self._header != DGROMessage.HEADER:
            message = "DGROParser expected dgro header {} at start of message but got {} instead".format(DGROMessage.HEADER, self._header)
            self.log.info("error: " + message)
            raise ValueError(message)
        return self._header

    def _parse_code(self):
        self._code = self._request[2*self.BYTE:4*self.BYTE]
        if self._code not in DGROMessage.valid_codes:
            message = "DGROParser expected one of these message type codes {} but got {} instead".format(DGROMessage.valid_codes, self._code)
            self.log.info("error: " + message)
            raise ValueError(message)
        return self._code

    def _parse_payload(self):
        self._payload = unword(bytes.fromhex(self._request[4*self.BYTE:len(self._request)]))
        if self.code in DGROMessage.header_only_codes:
            if self._payload:
                raise ValueError("DGROParser expected no payload for message type {} but got {} words instead".format(self.code, len(self._payload)))
        elif self.code in DGROMessage.address_only_codes:
            self._addresses = self._payload
        elif self.code in DGROMessage.data_codes:
            if len(self._payload) % 2 != 0:
                raise ValueError("DGROParser expected address-data pairs in payload for message type {}. Missing data for address {}".format(self.code, self._payload[-1]))
            self._addresses = self._payload[0::2]
            self._data = self._payload[1::2]

    def hex(self):
        return self._request


@has_log
class DGROBinaryMatcher(PatternMatcher):
    def __init__(self):
        super(DGROBinaryMatcher, self).__init__("accept header")

    @property
    def arg_count(self):
        return 1

    @property
    def argument_mappings(self):
        return None

    def match(self, request):
        try:
            self.log.debug("matching request = " + request.hex())
            p = DGROParser(request)
            self.log.debug(p._request)
            return [p];
        except ValueError as e:
            self.log.debug("error")
            self.log.debug(e)
            return None

@has_log
class UDPStreamHandler(StreamHandler):
    def __init__(self, sock, target, stream_server):
        try:
            asynchat.async_chat.__init__(self, sock=sock)
            self.set_terminator(b(target.in_terminator))
            self._readtimeout = target.readtimeout
            self._readtimer = 0
            self._target = target
            self._buffer = []

            self._stream_server = stream_server
            self._target.handler = self

            self._set_logging_context(target)
        except:
            self.log.debug(traceback.format_exc())

    def _send_reply(self,reply):
        if reply is not None:
            self.log.info('Sending reply %s', reply.hex())
            self.socket.send(reply)
            self.log.debug('Sent reply %s', reply.hex())

@has_log
class UDPHackSocket(socket.socket):
    def __init__(self, address):
        super(UDPHackSocket, self).__init__(socket.AF_INET, socket.SOCK_DGRAM)
        self.address = address

    def send(self, data):
        self.log.debug("sending" + data.hex())
        return self.sendto(data, self.address)

@has_log
class UDPServer(StreamServer):
    def __init__(self, host, port, target, device_lock):
        asyncore.dispatcher.__init__(self)
        self.target = target
        self.device_lock = device_lock
        self.create_socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.set_reuse_addr()
        self.bind((host, port))

        self._set_logging_context(target)
        self.log.info('Listening on %s:%s', host, port)

        self._accepted_connections = []
        self.handlers = {}

    def handle_read(self):
        try:
            data,address = self.socket.recvfrom(8192)
            self.log.debug("data = {} address={}".format(data.hex(), address))
            handler = self.handlers.get(address, None)
            if not handler:
                s = UDPHackSocket(address)
                handler = UDPStreamHandler(s, self.target, self)
                self.handlers[address] = handler
            handler.collect_incoming_data(data)

        except:
            self.log.debug(sys.exc_info())


    def process(self, msec):
        for address,handler in self.handlers.items():
            handler.process(msec)

@has_log
class UDPAdapter(StreamAdapter):
    def __init__(self, options=None):
        super(UDPAdapter, self).__init__(options)

    def start_server(self):
        if self._server is None:
            if self._options.telnet_mode:
                self.interface.in_terminator = '\r\n'
                self.interface.out_terminator = '\r\n'

            self._server = UDPServer(self._options.bind_address, self._options.port,
                                        self.interface, self.device_lock)




@has_log
class DGROStreamInterface(StreamInterface):
    protocol = "udpstream"

    commands = {
        Cmd('catchall', DGROBinaryMatcher(), return_mapping=lambda x: x)
    }

    # unset the terminators
    # An empty in_terminator triggers "timeout mode"
    # Otherwise, a ReadTimeout is considered an error.
    in_terminator = ''
    out_terminator = ''

    # Unusually long, for easier manual entry
    readtimeout = 10*1000 # microseconds, actually

    @property
    def adapter(self):
        return UDPAdapter

    def catchall(self, input):
        try:
            h = input.header
        except ValueError as e:
            self.log.debug(e)
            response = DGROMessage(DGROMessage.BAD_HEADER)
        try:
            code = input.code
        except ValueError as e:
            self.log.debug(e)
            response =  DGROMessage(DGROMessage.BAD_TYPE)

        if code == DGROMessage.READ_REQUEST:
            data = self.device.read(input.addresses)
            response = DGROMessage(DGROMessage.READ_RESPONSE, input.addresses, data)
        elif code == DGROMessage.WRITE_REQUEST:
            self.device.write(input.addresses, input.data)
            response = DGROMessage(DGROMessage.WRITE_RESPONSE, input.addresses, input.data)
        self.device.log_exchange(input.hex(), response.hex())
        return response.bytes()
