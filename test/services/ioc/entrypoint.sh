#!/usr/bin/env bash
source ~/.bashrc
conda activate ioc && \
cd test/services/ioc && \
source ./env.sh && \
iocsh.bash st.cmd
